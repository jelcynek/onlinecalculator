﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using onlinecalculator.Models;

namespace onlinecalculator.Repositories
{
    public interface IEquationRepository : IDisposable
    {
        IEnumerable<Equation> GetEquations();
        Equation GetEquationByID(int equationID);
        void InsertEquation(Equation equation);
        void DeleteEquation(int equationID);
        void UpdateEquation(Equation equation);
        void Save();
    }
}
