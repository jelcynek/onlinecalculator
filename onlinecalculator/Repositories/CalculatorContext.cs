﻿using onlinecalculator.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace onlinecalculator.Repositories
{
    public class CalculatorContext : DbContext
    {
        public DbSet<Equation> Equations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}