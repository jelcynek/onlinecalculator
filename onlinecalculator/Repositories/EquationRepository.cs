﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using onlinecalculator.Models;

namespace onlinecalculator.Repositories
{
    public class EquationRepository : IEquationRepository, IDisposable
    {
        private CalculatorContext context;

        public EquationRepository(CalculatorContext context)
        {
            this.context = context;
        }
        
        public IEnumerable<Equation> GetEquations()
        {
            return context.Equations.ToList();
        }

        public Equation GetEquationByID(int equationID)
        {
            return context.Equations.Find(equationID);
        }

        public void InsertEquation(Equation equation)
        {
            context.Equations.Add(equation);
        }

        public void DeleteEquation(int equationID)
        {
            Equation equation = context.Equations.Find(equationID);
            context.Equations.Remove(equation);
        }

        public void UpdateEquation(Equation equation)
        {
            context.Entry(equation).State = EntityState.Modified;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}