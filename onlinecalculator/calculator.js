﻿"use strict";

var firstValue, secondValue, operation, result;
var operationFunctionsMap = {
    '+': add,
    '-': subtract,
    'x': multiply,
    '/': divide
}

var operationValueMap = {
    '+': 0, '-': 1, 'x': 2, '/': 3
}

function init() {
    clearCalculatorValues();
    prepareButtons();
}
init();

function prepareButtons() {
    $(".calculator .number").click(function (e) {        
        selectNumber(e.target.innerText);
        setDisabledAttributeForButtons(false);
        updateViewValues();
    });
    $(".calculator .operator").click(function (e) {
        selectOperator(e.target.innerText);
        updateViewValues();
    });
    $(".calculator #calculate").click(function () {
        calculate();
        setDisabledAttributeForButtons(true);
        postEquation();
        updateViewValues();
    });
    $(".calculator #clear").click(function () {
        clearCalculatorValues();
        setDisabledAttributeForButtons(false);
        updateViewValues();
    });
    $(".calculator #negate").click(function () {
        negate();
        updateViewValues();
    });
    $(".calculator #delete").click(function () {
        delLastDigit();
        updateViewValues();
    });
    $(".calculator #period").click(function () {
        decimalPoint();
        updateViewValues();
    });
}

function setDisabledAttributeForButtons(value) {
    $(".calculator #period").attr('disabled', value);
    $(".calculator #delete").attr('disabled', value);
    $(".calculator #period").attr('disabled', value);
    $(".calculator #calculate").attr('disabled', value);
    $(".calculator #negate").attr('disabled', value);
    $(".calculator .operator").attr('disabled', value);
}

function selectNumber(number) {
    if (result) {
        clearCalculatorValues();
    }
    if (secondValue || operation) {
        if (number === '0' && !secondValue) {
            return;
        }
        secondValue += number;        
    }
    else {
        if (number === '0' && !firstValue) {
            return;
        }
        firstValue += number;
    }
}

function selectOperator(operationSign) {
    if (operation || !firstValue) {        
        return;
    }
    operation = operationSign;
    updateViewValues();
}

function updateViewValues() {
    var equationResult = result ? result : secondValue ? secondValue : firstValue;    
    if (!equationResult) {
        equationResult = '0';
    }
    $('#equation-result').text(equationResult);
    $('#equation').text(buildEquationString());    
}

function addHistoricEquations(equation) {
    var equationString = equation.a + ' ' + getOperationKeyByValue(equation.operation) + ' ' + equation.b + ' = ' + equation.result;
    $('#historic-equations').append('<p>' + equationString + '</p>');
}

function buildEquationString() {
    var equation = '';
    if (firstValue) {
        equation += firstValue;
    }
    if (operation) {
        equation += ' ' + operation;
    }
    if (secondValue) {
        equation += ' ' + secondValue;
    }
    if (result) {
        equation += ' = ' + result;
    }
    return equation;
}

function delLastDigit() {    
    if (result) {
        return
    }

    if (secondValue) {
        secondValue = secondValue.slice(0, -1);
    }
    else if (firstValue && !operation) {
        firstValue = firstValue.slice(0, -1);
    }
    return;
}

function negate() {
    if (result) {
        return
    }

    if (secondValue) {
        secondValue = -secondValue;
    }
    else if (firstValue && !operation) {
        firstValue = -firstValue;
    }
    return;
}

function decimalPoint() {
    if (result) {
        return
    }

    if (secondValue && secondValue.indexOf('.') == -1) {        
        secondValue += '.';
    }
    else if (firstValue && firstValue.indexOf('.') == -1) {
        firstValue += '.';
    }
    return;
}

function clearCalculatorValues() {
    operation = undefined;
    firstValue = secondValue = result = '';    
}

function add() {
    if (!firstValue || !secondValue) {
        return;
    }        
    result = parseFloat(firstValue) + parseFloat(secondValue);
}

function subtract() {
    if (!firstValue || !secondValue) {
        return;
    }
    result = parseFloat(firstValue) - parseFloat(secondValue);
}

function multiply() {
    if (!firstValue || !secondValue) {
        return;
    }
    result = parseFloat(firstValue) * parseFloat(secondValue);
}

function divide() {
    if (!firstValue || !secondValue) {
        return;
    }
    result = parseFloat(firstValue) / parseFloat(secondValue);
}

function calculate() {
    if (!operation || !operationFunctionsMap[operation]) {
        return;
    }
    operationFunctionsMap[operation]();
}

function postEquation() {    
    if (!firstValue || !secondValue || !result || !operation) {
        return false;
    }

    var equation = {
        'a': firstValue,
        'b': secondValue,
        'result': result,
        'operation': operationValueMap[operation]
    };

    $.ajax({
        type: 'POST',
        data: JSON.stringify(equation),
        url: 'api/equation',
        contentType: 'application/json'
    });
    addHistoricEquations(equation);
}

function getEquations() {
    $.get('api/equation', function (data) {
        data.forEach(function (equation) {
            addHistoricEquations(equation);
        });
    });
}
getEquations();

function getOperationKeyByValue(value) {
    for(var prop in operationValueMap) {
        if(operationValueMap.hasOwnProperty(prop)) {
            if (operationValueMap[prop] === value) {
                return prop;
            }
        }
    }
}
