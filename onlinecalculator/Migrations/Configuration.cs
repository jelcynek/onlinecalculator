namespace onlinecalculator.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using onlinecalculator.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<onlinecalculator.Repositories.CalculatorContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(onlinecalculator.Repositories.CalculatorContext context)
        {
            var equations = new List<Equation>
            {
                new Equation { a = 11, b = 22, result = 33, operation = Operation.Add }
            };
            equations.ForEach(eq => context.Equations.AddOrUpdate(e => e.EquationID, eq));
            context.SaveChanges();    
        }
    }
}
