namespace onlinecalculator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Equation",
                c => new
                    {
                        EquationID = c.Int(nullable: false, identity: true),
                        a = c.Decimal(nullable: false, precision: 18, scale: 2),
                        b = c.Decimal(nullable: false, precision: 18, scale: 2),
                        result = c.Decimal(nullable: false, precision: 18, scale: 2),
                        operation = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EquationID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Equation");
        }
    }
}
