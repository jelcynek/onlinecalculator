﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using onlinecalculator.Models;
using onlinecalculator.Repositories;

namespace onlinecalculator.Controllers
{    
    public class EquationController : ApiController
    {
        private IEquationRepository equationRepository;

        public EquationController()
        {
            this.equationRepository = new EquationRepository(new CalculatorContext());
        }

        public EquationController(IEquationRepository equationRepository)
        {
            this.equationRepository = equationRepository;
        }

        // GET: /
        public IEnumerable<Equation> GetAllEquations()
        {
            return equationRepository.GetEquations();
        }

        public IHttpActionResult PostEquation([FromBody] Equation equation)
        {          
            equationRepository.InsertEquation(equation);
            equationRepository.Save();
            return Ok();
        }
    }
}
