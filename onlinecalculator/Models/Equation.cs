﻿using System;
using System.ComponentModel.DataAnnotations;

namespace onlinecalculator.Models
{
    public class Equation
    {
        [Key]
        public int EquationID { get; set; }
        public Decimal a { get; set; }
        public Decimal b { get; set; }
        public Decimal result { get; set; }
        public Operation operation { get; set; }
    }

    public enum Operation
    {
        Add = 0,
        Subtract,
        Multiply, 
        Divide
    }
}