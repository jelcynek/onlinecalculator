# README #

### Quick summary
...Online calculator with saving historical equations in database functionality using REST Api.

## How do I get set up? ###
### Summary of set up
...Easiest way from Visual Studio. Just start application.

### Dependencies
*ASP.NET MVC
*Entity Framework
*jQuery
*Bootstrap
*MS SQL Server Express 2012

### Database configuration
...To set up database run update-database from Package Manager Console in Visual Studio. It will create db schema and seed data.

### How to run tests
...TBD

### REST API
* /api/equation, method: GET, description: Get all historic equations.
* /api/equation, method: POST, description: Post equation to be saved in db.

